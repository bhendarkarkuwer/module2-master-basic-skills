# **FACE RECOGNITION :**

Face recognition is a method of identifying or verifying the identity of an individual using their face. Face recognition systems can be used to identify people in photos, video, or in real-time. Law enforcement may also use mobile devices to identify people during police stops.

It is a method of biometric identification that uses that body measures, in this case face and head, to verify the identity of a person through its facial biometric pattern and data.

![alt text](https://newsroom.cisco.com/documents/10157/14740/facial-recognition-feature_1200x675_hero_090418.jpg/eba19a1c-4b85-413d-a1fc-2590fcd7e1ee "Title Text")

## **FACE RECOGNITION SYSTEM**

The software identifies 80 nodal points on a human face. In this context, nodal points are endpoints used to measure variables of a person’s face, such as the length or width of the nose, the depth of the eye sockets and the shape of the cheekbones. The system works by capturing data for nodal points on a digital image of an individual’s face and storing the resulting data as a faceprint. The faceprint is then used as a basis for comparison with data captured from faces in an image or video.

![alt text](https://ars.els-cdn.com/content/image/1-s2.0-S2590005619300141-fx1.jpg "Title Text")




## **Examples of facial recognition**

- High-quality cameras in mobile devices have made facial recognition a viable option for authentication as well as identification.
- Facebook uses facial recognition software to tag individuals in photographs.
- government at airports, via The Department of Homeland Security, to identify individuals who may overstay their visas.
- Marketing, where marketers can use facial recognition to determine age, gender and ethnicity to target specific audiences.

## **Benefits**
- No need to physically contact a device for authentication
- Improved level of security.
- Requires less processing compared to other biometric authentication techniques.
Easy to integrate with existing security features.

![alt text](https://www.cleveroad.com/images/article-previews/apple-face-recognition-software.png "Title Text")





