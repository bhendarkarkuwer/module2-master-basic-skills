# **Object Detection**

Object recognition is a general term to describe a collection of related computer vision tasks that involve identifying objects in digital photographs.

![alt text](https://miro.medium.com/max/700/1*j_zE5G5zttpWLd5hXsv0jA.jpeg "Title Text")

## **Two-Step Object Detection**

The first step requires a Region Proposal Network, providing a number of regions which are then passed to common DL based classification architectures. From the hierarchical grouping algorithm in RCNNs (which are extremely slow) to using CNNs and ROI pooling in Fast RCNNs and anchors in Faster RCNNs (thus speeding up the pipeline and training end-to-end), a lot of different methods and variations have been provided to these region proposal networks (RPNs).

![alt text](https://miro.medium.com/max/548/1*NXWE7BHug0i-FQlHo5xa7w.png "Title Text")

## **One-Step Object Detection**

With the need of real time object detection, many one-step object detection architectures have been proposed, like YOLO, YOLOv2, YOLOv3, SSD, RetinaNet etc. which try to combine the detection and classification step.

![alt text](https://miro.medium.com/max/600/1*CYTDLg54ol-NpBOnrhFo2A.jpeg "Title Text")

## **Examples of object detection**

- activity recognition
- tracking objects
- image annotation

![alt text](https://cdn.analyticsvidhya.com/wp-content/uploads/2020/04/od3.png "Title Text")

## **benfits of Object Detection**

- Sports broadcasting 
- video collaboration
-security industry.







